﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empadão_do_Golphinho.Business
{
    class BusinessEscola
    {
        public void Inserir(Database.Entity.tb_turma turma)
        {
            if (string.IsNullOrEmpty(turma.nm_turma) == false && string.IsNullOrEmpty(turma.nm_curso) == false)
            {
                Database.DataBaseEscola data = new Database.DataBaseEscola();
                data.Inserir(turma);
            }
            else
            {
                MessageBox.Show("Preencha todos os campos corretamentes");
            }
        }
        public List<Database.Entity.tb_turma> consultar(string Curso)
        {
            Database.DataBaseEscola data = new Database.DataBaseEscola();
            List<Database.Entity.tb_turma> lista = data.Consultar(Curso);
            return lista;
        }
        public List<Database.Entity.tb_turma> consultar2()
        {
            Database.DataBaseEscola data = new Database.DataBaseEscola();
            List<Database.Entity.tb_turma> lista = data.Consultar2();
            return lista;
        }
        public List<Database.Entity.tb_turma> consultar3(int ID)
        {
            Database.DataBaseEscola data = new Database.DataBaseEscola();
            List<Database.Entity.tb_turma> lista = data.Consultar3(ID);
            return lista;
        }
        public List<Database.Entity.tb_turma> consultar4(string turma)
        {
            Database.DataBaseEscola data = new Database.DataBaseEscola();
            List<Database.Entity.tb_turma> lista = data.Consultar4(turma);
            return lista;
        }
        public void Remover(int ID)
        {
            if (String.IsNullOrEmpty(Convert.ToString(ID)) == false)
            {
                Database.DataBaseEscola data = new Database.DataBaseEscola();
                data.Remover(ID);
            }
            else
            {
                MessageBox.Show("Preencha todos os campos corretamentes");
            }
        }
        public void Editar(Database.Entity.tb_turma turma)
        {
            if (String.IsNullOrEmpty(turma.nm_turma) == false && String.IsNullOrEmpty(turma.nm_curso) == false && String.IsNullOrEmpty(Convert.ToString(turma.id_turma)) == false)
            {
                Database.DataBaseEscola data = new Database.DataBaseEscola();
                data.Editar(turma);
            }
            else
            {
                MessageBox.Show("Preencha todos os campos corretamentes");
            }
        }
        public void Editar2(Database.Entity.tb_turma turma,string nome)
        {
            if (String.IsNullOrEmpty(turma.nm_turma) == false && String.IsNullOrEmpty(turma.nm_curso) == false && String.IsNullOrEmpty(nome) == false)
            {
                Database.DataBaseEscola data = new Database.DataBaseEscola();
                data.Editar2(turma,nome);
            }
            else
            {
                MessageBox.Show("Preencha todos os campos corretamentes");
            }
        }
        public void Editar3(Database.Entity.tb_turma turma, string nome)
        {
            if (String.IsNullOrEmpty(turma.nm_turma) == false && String.IsNullOrEmpty(turma.nm_curso) == false && String.IsNullOrEmpty(nome) == false)
            {
                Database.DataBaseEscola data = new Database.DataBaseEscola();
                data.Editar3(turma, nome);
            }
            else
            {
                MessageBox.Show("Preencha todos os campos corretamentes");
            }
        }
        public void Remover2(string nome)
        {
            if (String.IsNullOrEmpty(nome) == false)
            {
                Database.DataBaseEscola data = new Database.DataBaseEscola();
                data.Remover2(nome);
            }
            else
            {
                MessageBox.Show("Preencha todos os campos corretamentes");
            }
        }
        public void Remover3(string nome)
        {
            if (String.IsNullOrEmpty(nome) == false)
            {
                Database.DataBaseEscola data = new Database.DataBaseEscola();
                data.Remover3(nome);
            }
            else
            {
                MessageBox.Show("Preencha todos os campos corretamentes");
            }
        }
    }
}
