﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empadão_do_Golphinho.Telas
{
    public partial class Remover2 : Form
    {
        public Remover2()
        {
            InitializeComponent();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            string turma = txtTurma.Text;
            Business.BusinessEscola busi = new Business.BusinessEscola();
            busi.Remover2(turma);
        }

        private void Lblexit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            Form1 tela = new Form1();
            tela.Show();
            this.Close();
        }
    }
}
