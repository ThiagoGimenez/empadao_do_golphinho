﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empadão_do_Golphinho
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cboMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            string escolha = cboMenu.Text;
            if (escolha == "Cadastrar")
            {
                Telas.Cadastrar tela = new Telas.Cadastrar();
                tela.Show();
                this.Hide();
            }
            if (escolha == "Consultar")
            {
                Telas.Consultar tela = new Telas.Consultar();
                tela.Show();
                this.Hide();
            }
        }
    }
}
