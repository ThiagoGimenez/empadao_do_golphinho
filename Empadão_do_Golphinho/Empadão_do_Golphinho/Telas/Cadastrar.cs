﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empadão_do_Golphinho.Telas
{
    public partial class Cadastrar : Form
    {
        public Cadastrar()
        {
            InitializeComponent();
        }

        private void lblexit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Btncadastrar_Click(object sender, EventArgs e)
        {
            Database.Entity.tb_turma model = new Database.Entity.tb_turma();
            model.nm_curso = cboCurso.Text;
            model.nm_turma = txtTurma.Text.Trim();
            model.qt_max_alunos = Convert.ToInt32(nudQaunt.Value);
            Business.BusinessEscola db = new Business.BusinessEscola();
            db.Inserir(model);
        }
        private void PictureBox1_Click(object sender, EventArgs e)
        {
            Form1 tela = new Form1();
            tela.Show();
            this.Close();
        }
    }
}
