﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empadão_do_Golphinho.Telas
{
    public partial class Remover : Form
    {
        public Remover()
        {
            InitializeComponent();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(txtID.Text);
            Business.BusinessEscola busi = new Business.BusinessEscola();
            busi.Remover(id);
        }

        private void Lblexit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
            Form1 tela = new Form1();
            tela.Show();
            this.Close();
        }
    }
}
