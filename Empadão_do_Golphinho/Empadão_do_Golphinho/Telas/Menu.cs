﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empadão_do_Golphinho
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cboMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            string escolha = cboMenu.Text;
            if (escolha == "Cadastrar")
            {
                Telas.Cadastrar tela = new Telas.Cadastrar();
                tela.Show();
                this.Hide();
            }
            if (escolha == "Consultar")
            {
                Telas.Consultar tela = new Telas.Consultar();
                tela.Show();
                this.Hide();
            }
            if (escolha == "Consultar2")
            {
                Telas.Consultar2 tela = new Telas.Consultar2();
                tela.Show();
                this.Hide();
            }
            if (escolha == "Consultar3")
            {
                Telas.Consultar3 tela = new Telas.Consultar3();
                tela.Show();
                this.Hide();
            }
            if (escolha == "Consultar4")
            {
                Telas.Consultar4 tela = new Telas.Consultar4();
                tela.Show();
                this.Hide();
            }
            if (escolha == "Remover")
            {
                Telas.Remover tela = new Telas.Remover();
                tela.Show();
                this.Hide();
            }
            if (escolha == "Editar")
            {
                Telas.Editar tela = new Telas.Editar();
                tela.Show();
                this.Hide();
            }
            if (escolha == "Editar2")
            {
                Telas.Editar2 tela = new Telas.Editar2();
                tela.Show();
                this.Hide();
            }
            if (escolha == "Editar3")
            {
                Telas.Editar3 tela = new Telas.Editar3();
                tela.Show();
                this.Hide();
            }
            if (escolha == "Remover2")
            {
                Telas.Remover2 tela = new Telas.Remover2();
                tela.Show();
                this.Hide();
            }
            if (escolha == "Remover3")
            {
                Telas.Remover3 tela = new Telas.Remover3();
                tela.Show();
                this.Hide();
            }
        }
    }
}
