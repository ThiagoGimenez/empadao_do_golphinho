﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empadão_do_Golphinho.Telas
{
    public partial class Editar : Form
    {
        public Editar()
        {
            InitializeComponent();
        }
        private void lblexit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        private void PictureBox1_Click(object sender, EventArgs e)
        {
            Form1 tela = new Form1();
            tela.Show();
            this.Close();
        }
        private void BtnEditar_Click(object sender, EventArgs e)
        {
            try
            {
                Database.Entity.tb_turma model = new Database.Entity.tb_turma
                {
                    id_turma = Convert.ToInt32(txtID.Text),
                    nm_curso = cboCurso.Text,
                    nm_turma = txtTurma.Text.Trim(),
                    qt_max_alunos = Convert.ToInt32(nudQaunt.Value)
                };
                Business.BusinessEscola db = new Business.BusinessEscola();
                db.Editar(model);
                
            }
            catch (Exception)
            {
                throw new ArgumentException("Preencha os campos corretamente");
            }
        }
    }
}
