﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Empadão_do_Golphinho.Database
{
    class DataBaseEscola
    {
        public void Inserir(Entity.tb_turma turma)
        {
            try
            {
                Entity.SCHOOLDBEntities db = new Entity.SCHOOLDBEntities();
                string exist = Convert.ToString(db.tb_turma.Where(l => l.nm_turma == turma.nm_turma));
                bool contem = string.IsNullOrEmpty(exist) || string.IsNullOrWhiteSpace(exist) || exist.Length > 50;
                if (contem == true)
                {
                    db.tb_turma.Add(turma);
                    db.SaveChanges();
                    MessageBox.Show("Perfil criado");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Turma já existente");
            }
        }
        public List<Entity.tb_turma> Consultar(string curso)
        {
            Entity.SCHOOLDBEntities db = new Entity.SCHOOLDBEntities();
            List<Entity.tb_turma> turma = db.tb_turma.Where(p => p.nm_curso == curso).ToList();
            return turma;
        }
        public List<Entity.tb_turma> Consultar2()
        {
            Entity.SCHOOLDBEntities db = new Entity.SCHOOLDBEntities();
            List<Entity.tb_turma> turma = db.tb_turma.ToList();
            return turma;
        }
        public List<Entity.tb_turma> Consultar3(int id)
        {
            Entity.SCHOOLDBEntities db = new Entity.SCHOOLDBEntities();
            List<Entity.tb_turma> turma = db.tb_turma.Where(p => p.id_turma == id).ToList();
            return turma;
        }
        public List<Entity.tb_turma> Consultar4(string Nometurma)
        {
            Entity.SCHOOLDBEntities db = new Entity.SCHOOLDBEntities();
            List<Entity.tb_turma> turma = db.tb_turma.Where(p => p.nm_turma == Nometurma).ToList();
            return turma;
        }
        public void Remover(int ID)
        {
            Entity.SCHOOLDBEntities db = new Entity.SCHOOLDBEntities();
            bool contem = Convert.ToBoolean(db.tb_turma.Find(ID));
            if (contem == true)
            {
                Entity.tb_turma Remover = db.tb_turma.First(i => i.id_turma == ID);
                db.tb_turma.Remove(Remover);
                db.SaveChanges();
                MessageBox.Show("Perfil Removido com sucesso!");
            }
            else
            {
                MessageBox.Show("ID não existente");
            }
        }
        public void Editar(Entity.tb_turma turma)
        {
            Entity.SCHOOLDBEntities db = new Entity.SCHOOLDBEntities();
            bool contem = Convert.ToBoolean(db.tb_turma.Find(turma.id_turma));
            if (contem == true)
            {
                Entity.tb_turma edit = db.tb_turma.First(e => e.id_turma == turma.id_turma);
                edit.nm_curso = turma.nm_curso;
                edit.nm_turma = turma.nm_turma;
                edit.qt_max_alunos = turma.qt_max_alunos;
                db.SaveChanges();
                MessageBox.Show("Edição concluida");
            }
            else
            {
                MessageBox.Show("ID não existente");
            }
        }
        public void Editar2(Entity.tb_turma turma,string nome)
        {
            Entity.SCHOOLDBEntities db = new Entity.SCHOOLDBEntities();
            string exist = Convert.ToString(db.tb_turma.First(p => p.nm_turma == nome));
            bool contem = string.IsNullOrEmpty(exist);
            if (contem != true)
            {
                Entity.tb_turma edit = db.tb_turma.First(e => e.nm_turma == nome);
                edit.nm_curso = turma.nm_curso;
                edit.nm_turma = turma.nm_turma;
                edit.qt_max_alunos = turma.qt_max_alunos;
                db.SaveChanges();
                MessageBox.Show("Edição concluida");
            }
            else
            {
                MessageBox.Show("Turma não existente");
            }
        }
        public void Editar3(Entity.tb_turma turma, string nome)
        {
            Entity.SCHOOLDBEntities db = new Entity.SCHOOLDBEntities();
            string exist = Convert.ToString(db.tb_turma.First(p => p.nm_curso == nome));
            bool contem = string.IsNullOrEmpty(exist);
            if (contem != true)
            {
                Entity.tb_turma edit = db.tb_turma.First(e => e.nm_curso == nome);
                edit.nm_curso = turma.nm_curso;
                edit.nm_turma = turma.nm_turma;
                edit.qt_max_alunos = turma.qt_max_alunos;
                db.SaveChanges();
                MessageBox.Show("Edição concluida");
            }
            else
            {
                MessageBox.Show("Turma não existente");
            }
        }
        public void Remover2(string nome)
        {
            try
            {
                Entity.SCHOOLDBEntities db = new Entity.SCHOOLDBEntities();
                string exist = Convert.ToString(db.tb_turma.First(p => p.nm_turma == nome));
                bool contem = string.IsNullOrEmpty(exist);
                if (contem != true)
                {
                    Entity.tb_turma Remover = db.tb_turma.First(i => i.nm_turma == nome);
                    db.tb_turma.Remove(Remover);
                    db.SaveChanges();
                    MessageBox.Show("Perfil Removido com sucesso!");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Turma não existente");
            }
        }
        public void Remover3(string nome)
        {
            try
            {
                Entity.SCHOOLDBEntities db = new Entity.SCHOOLDBEntities();
                string exist = Convert.ToString(db.tb_turma.First(p => p.nm_curso == nome));
                bool contem = string.IsNullOrEmpty(exist);
                if (contem != true)
                {
                    Entity.tb_turma Remover = db.tb_turma.First(i => i.nm_curso == nome);
                    db.tb_turma.Remove(Remover);
                    db.SaveChanges();
                    MessageBox.Show("Perfil Removido com sucesso!");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Turma não existente");
            }

        }
    }
}
