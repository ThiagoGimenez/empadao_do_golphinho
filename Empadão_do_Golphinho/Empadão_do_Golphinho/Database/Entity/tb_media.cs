//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Empadão_do_Golphinho.Database.Entity
{
    using System;
    using System.Collections.Generic;
    
    public partial class tb_media
    {
        public int id_media { get; set; }
        public string ds_bimestre { get; set; }
        public decimal vl_nota1 { get; set; }
        public decimal vl_nota2 { get; set; }
        public decimal vl_nota3 { get; set; }
        public decimal vl_media { get; set; }
        public int id_aluno { get; set; }
    }
}
